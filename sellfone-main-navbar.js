import { html, LitElement } from 'lit-element';
import style from './sellfone-main-navbar-styles.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icon/iron-icon.js';
import '@catsys/sellfone-cart-description';

class SellfoneMainNavbar extends LitElement {
  static get properties() {
    return {
      navbarLinks:{
        type:Array,
        attribute:"navbar-links"
      },
      productArray:{
        type:Array,
        attribute:"product-array"
      },
      favoritesArray:{
        type:Array,
        attribute:"favorites-array"
      },
      logoImage:{
        type:String,
        attribute:"logo-image"
      },
      openedSidebar:{
        type:Boolean,
        attribute:"opened-sidebar"
      },
      productArray:{
        type:Object,
        attribute:"product-array"
      }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.navbarLinks=[];
    this.productArray=[];
    this.favoritesArray=[];
    this.openedSidebar=false;
  }

  render() {
    return html`
        <div class="navbarContainer"> 
              <div class="imageContainer">
               <img class="logo" src="${this.logoImage}" alt="">
              </div>
              <div class="searchContainer">
                <input class="upperItem" type="text" placeholder="Search for product" id="searchInput">
                <button class="upperItem upperButton" id="searchButton" @click="${this.searchProduct}">
                    <iron-icon icon="search"></iron-icon>
                </button>
              </div>
              <div class="buttonsContainer">
               <button class="upperItem upperButton" id="sellButton" @click="${this.sellMyPhone}">
                   Vender mi cel
               </button>
             </div>

        </div>
        <div class="navbarContainer2">
           <div class="subContainer" style="border:none; height:25px;">
           ${this._iterateLinks()}
           </div>
        </div>
        </div>
        <div class="responsiveNavContainer">
          <img class="responsiveLogo" src="${this.logoImage}" alt="">
          <div class="rightContainer">
          <button class="burguer" id="favoritesButton">
                  <iron-icon icon="shopping-cart"></iron-icon>
                  ${this.productArray.length>0 ? html`<sup class="sups">${this.productArray.length}</sup>`:html``}
                  </button>
          <button class="burguer" id="favoritesButton">
                   <iron-icon icon="favorite"></iron-icon>
                   ${this.favoritesArray.length>0 ? html`<sup class="sups">${this.favoritesArray.length}</sup>`:html``}
                  
          </button>
          <button class="burguer" @click="${this.toggleSidebar}"> <iron-icon icon="menu"></iron-icon></button>
          </div>
        </div>
        <div class="sidebarContainer">
          <div class="sidebarItem invisibleSidebarItem">
          <p @click=${this.toggleSidebar} class="closeButton ">X</p>
            </div>
            <div class="sidebarItem invisibleSidebarItem">
              <input id="sidebarSearch" placeholder="Search for a product">
            </div>
            ${this._iterateResponsiveLinks()}
        </div>
      `;
    }

    firstUpdated(){
      const navbarDesctiptions=this.shadowRoot.querySelectorAll("sellfone-cart-description");
      navbarDesctiptions.forEach(cart=>{
        cart.productArray=this.productArray;
      })
    }

    searchProduct(){
      const input=this.shadowRoot.querySelector("#searchInput");
      this.dispatchEvent(new CustomEvent("search-product",{
        bubbles:false,
        composed:false,
        detail:input.value
      }));
    }

    _iterateLinks(){
      return html`
        ${this.navbarLinks.map(link=>{
          return html`${link.subLinks.length>0? html`<div class="dropdown2">
                <a href="${link.href}" class="links">${link.title}</a>
                <div class="dropdown-content2 animated fadeIn">
                   <div class="dropdownContentContainer">
                    ${link.subLinks.map(subLink=>{
                    return html` <div class="linkContainer">
                      <a href="${subLink.href}" class="subLinks">${subLink.title}</a>
                      <div class="linkDropdown animated fadeIn">
                        <a href="${subLink.href}"><img class="navImage" src="${subLink.productImg}" alt=""></a>
                      </div>
                     </div>`
                     })}
                   </div>
                  
                </div>
            </div>`:html`<a href="#" class="links">${link.title}</a>`}`
        })}

      `
    }

    _iterateResponsiveLinks(){
     return html` ${this.navbarLinks.map((link,index)=>{
        return html`<div class="sidebarItem invisibleSidebarItem">
          ${link.subLinks.length>0? html`<a href="${link.href}" class="sidebarLink ">${link.title}</a> <iron-icon icon="expand-more" class="expandIcon" @click=${(e)=>this.toggleSubSideContainer(index)}></iron-icon>
          <div class="subItemsSideContainer invisibleSub">
          ${link.subLinks.map(subLink=>{
            return html`<div class="subItem ">
               <a href="${subLink.href}" class="subLink">${subLink.title}</a>
            </div>`
          })}
          </div>`:
          html`
          <a href="${link.href}" class="sidebarLinkNoIcon">${link.title}</a>
           <div class="subItemsSideContainer invisibleSub"></div>
          `}
          </div>`
      })}`
    }

    toggleSidebar(){
      const sidebar=this.shadowRoot.querySelector(".sidebarContainer");
      sidebar.classList.toggle("sidebarContainerVisible");
      const links=this.shadowRoot.querySelectorAll(".sidebarItem ");
       for(const link of links){
         link.classList.toggle("invisibleSidebarItem");
       }
       this.openedSidebar=!this.openedSidebar;
       this.dispatchEvent(new CustomEvent("opened-sidebar",{
         bubbles:false,
         composed:false,
         detail:this.openedSidebar
       }))
      
    }

    toggleSubSideContainer(index){
      const subItems=this.shadowRoot.querySelectorAll(".subItemsSideContainer");
      subItems[index].classList.toggle("invisibleSub");
      subItems[index].classList.toggle("animated2");
      subItems[index].classList.toggle("bounceInDown");
      this.dispatchEvent(new CustomEvent("subcontainer-toggled",{
        bubbles:false,
        composed:false,
        detail:this.navbarLinks.subItems[index]
      }))
    }

    cartClicked(){
      this.dispatchEvent(new CustomEvent("cart-clicked",{
        bubbles:false,
        composed:false,
        detail:this.productArray
      }))
    }

    sellMyPhone(){
      this.dispatchEvent(new CustomEvent("sell-phone",{
        bubbles:false,
        composed:false,
        detail:null
      }));
    }

    seeFavorites(){
      this.dispatchEvent(new CustomEvent("see-favorites",{
        bubbles:false,
        composed:false,
        detail:this.favoritesArray
      }));
    }

}

window.customElements.define("sellfone-main-navbar", SellfoneMainNavbar);


