/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-main-navbar.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-main-navbar></sellfone-main-navbar>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
