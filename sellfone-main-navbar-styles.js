import { css } from 'lit-element';

export default css`
:host {
  display: inline-block;
  box-sizing: border-box;
  width: 100%;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

*,
*:before,
*:after {
  box-sizing: inherit;
  font-family: inherit;
}

.navbarContainer {
  display: grid;
  grid-template-columns: 30% 40% 30%;
  background-color: #00305E;
  padding: 10px;
  align-items: center;
  justify-items: flex-end;
}

.searchContainer{
  display: flex;
  flex-flow: row;
  justify-content: center;
  width: 100%;
}

.imageContainer{
  width: 100%;
}

.navbarContainer2{
  background-color: #FFFFFF;
  padding: 20px 0;
}

.subContainer {
  display: flex;
  width: 100%;
  flex-flow: row nowrap;
  justify-content: center;
}


.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  padding: 12px 16px;
  z-index: 1;
  right: 1px;
}

.dropdown-content2 {
  display: none;
  position: absolute;
  max-width: 500px;
  background-color: white;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.2);
  padding: 0px 5px;
  z-index: 1;
}

.dropdown2:hover .dropdown-content2 {
  display: flex;
  flex-flow: row wrap;
  max-width: 100px;
  
}

.dropdown:hover .dropdown-content {
  display: block;
}

.dropdownContentContainer {
  display: flex;
  flex-flow: row wrap;
}

.cont2 {
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-end;
  align-items: center;
}

.upperButton {
  color: white;
  border: none;
  transition: 0.2s;
  background-color: rgba(0, 0, 0, 0);
  cursor: pointer;
  outline: none;
  
}

.buttonsContainer{
  width: 100%;
  text-align: center;
}

#sellButton{
  background-color: rgba(0, 0, 0, 0);
  color: #FF8800;
  border: 1px solid #FF8800;
  width: 40%;
  padding: 10px 10px;
  font-size: 110%;
  border-radius: 5px;
  transition: 0.3s;
}

#sellButton:hover{
  background-color:#FF8800;
  color: white;
}

#searchInput {
  border: none;
  outline: none;
  color: black;
  align-self: center;
  height: 25px;
  width: 98%;
  padding: 10px;
  background-color: white;
  border-bottom: 1px solid white;
  font-size: 20px;
}

#searchButton{
  background-color: #FF8800;
  margin-left: 5px;
  font-weight: bold;
}

.links {
  color: black;
  text-decoration: none;
  font-size: 125%;
  margin: 0 10px;
  transition: 0.3s;
  border-bottom: 1px solid rgba(0, 0, 0, 0);
}

.links:hover {
  border-bottom: 2px solid white;
}

.linkContainer{
  padding: 10px;
  width: 100%;
  cursor: pointer;
  position: relative;
  display: inline-block;
  transition: 0.5s;
}


.linkDropdown{
  display: none;
  position: absolute;
  text-align: center;
  margin-left: 80px;
  max-width: 200px;
  max-height: 200px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  padding: 0px 15px;
  z-index: 1;
  margin-top: -20%;
}

.linkContainer:hover .linkDropdown {
  display: block;
}

.subLinks {

  text-decoration: none;
  color: black;
  transition: 0.5s;
  font-size: 110%;
}

.sups {
  background-color: rgb(255, 55, 55);
  padding: 2px;
  font-size: 120%;
  border-radius: 100%;
  width: 20px;
  height: 20px;
  display: inline-block;
}

.responsiveNavContainer {
  display: none;
  background-color: rgb(0, 48, 94);
}

.responsiveLogo {
  max-width: 100%;
}

.logo {
  max-height: 60px;
}

.burguer {
  background-color: rgba(0, 0, 0, 0);
  outline: none;
  height: 30px;
  align-self: center;
  border: none;
  color: white;
  cursor: pointer;
}

.sidebarContainer {
  position: absolute;
  top: 0;
  width: 0;
  z-index: 2;
  background-color: rgb(0, 48, 94);
  height: 100vh;
  transition: width 0.35s;
}

.sidebarContainerVisible {
  display: block;
  visibility: visible;
  width: 350px;

}

.rightContainer {
  display: flex;
}

.sidebarItem {
  padding: 10px 0px;
  width: 100%;
  text-align: center;
}

#sidebarSearch {
  background-color: rgba(0, 0, 0, 0);
  border: none;
  width: 80%;

  border-bottom: 2px solid white;
  outline: none;
  height: 30px;
  font-size: 125%;
  color: white;
}

#sidebarSearch ::placeholder {
  color: rgb(196, 195, 195);
}



.sidebarLink {
  visibility: visible;
  color: rgba(255, 255, 255, 0.7);
  font-size: 125%;
  text-decoration: none;
  transition: 0.3s;
  cursor: pointer;
  background-color: rgba(0, 0, 0, 0);
  border: none;
  font-weight: bold;
}

.sidebarLinkNoIcon {
  visibility: visible;
  color: rgba(255, 255, 255, 0.7);
  font-size: 125%;
  text-decoration: none;
  transition: 0.3s;
  cursor: pointer;
  background-color: rgba(0, 0, 0, 0);
  border: none;
  font-weight: bold;
  margin-right: 27px;
}

.invisibleSidebarItem {
  display: none;
}

.sidebarLink:hover {
  color: white;
}

.sidebarLinkNoIcon:hover {
  color: white;
}

.dropdownResponsive {
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  padding: 0px 5px;
  z-index: 1;
}

.dropdownResponsive:focus .dropdown-content2 {
  display: block;
}

.closeContainer {
  display: inline;
}

.closeButton {
  float: right;
  width: 100%;
  text-align: right;
  font-size: 125%;
  transition: 0.2s;
  cursor: pointer;
  margin-right: 15px;
  color: white;
}

.subItemsSideContainer {
  width: 100%;
  background-color: #0b3b66;
}

.subItem {
  padding: 10px;
}

.subLink {
  text-decoration: none;
  color: rgba(248, 248, 248, 0.664);
  cursor: pointer;
}

iron-icon {
  cursor: pointer;
  transition: 0.3s;
}


iron-icon:hover {
  color: white;
}

.invisibleSub {
  display: none;
}

.dropdownImg {
  max-width: 100%;
}

.expandIcon{
  color: white;
}

.cart-description{
 width: 400px;
}

.navImage{
  max-width: 100px;

}

.animated {
  -webkit-animation-duration: 0.5s;
  animation-duration: 0.5s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

@-webkit-keyframes fadeIn {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}

@keyframes fadeIn {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}

.fadeIn {
  -webkit-animation-name: fadeIn;
  animation-name: fadeIn;
}

@-webkit-keyframes bounceInDown {

  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -3000px, 0);
    transform: translate3d(0, -3000px, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(0, 25px, 0);
    transform: translate3d(0, 25px, 0);
  }

  75% {
    -webkit-transform: translate3d(0, -10px, 0);
    transform: translate3d(0, -10px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, 5px, 0);
    transform: translate3d(0, 5px, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes bounceInDown {

  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -3000px, 0);
    transform: translate3d(0, -3000px, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(0, 25px, 0);
    transform: translate3d(0, 25px, 0);
  }

  75% {
    -webkit-transform: translate3d(0, -10px, 0);
    transform: translate3d(0, -10px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, 5px, 0);
    transform: translate3d(0, 5px, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.bounceInDown {
  -webkit-animation-name: bounceInDown;
  animation-name: bounceInDown;
}



@media only screen and (max-width: 750px) {
  .responsiveLogo {
    max-height: 60px;
  }

  .navbarContainer {
    display: none;
  }

  .responsiveNavContainer {
    padding: 10px;
    display: flex;
    flex-flow: row nowrap;
    width: 100%;
    justify-content: space-between;
  }
}

@media only screen and (max-width: 460px) {
  .responsiveLogo {
    height: 30px;
  }

}

@media only screen and (max-width: 350px) {
  .sidebarContainerVisible {
    width: 250px;
  }

}
`;
